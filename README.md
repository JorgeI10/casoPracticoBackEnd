# Caso Práctico

Para ejecutar el proyecto localmente, es necesario ejecutar abrir dos terminales.

En la primera terminal ingresar a la ubicación del proyecto /frontend 
En la segunda terminal ingresar a la ubicación del proyecto /backend

Posteriormente ejecutar 

```npm install```

en cada una de las terminales, para instalar las dependencias.

Finalmente,
en la terminal de frontend ejecutar 
```npm start```

en la terminal de backend
```npm start``` o ```npm run dev```


# Usuarios

Puedes crear un nuevo usuario o usar uno existente

Email: correo@correo.com
Password: 123456
