const Usuario = require('../models/Usuario');
const bcryptjs = require('bcryptjs');
const { validationResult } = require('express-validator');
const jwt = require('jsonwebtoken');

exports.autenticarUsuario = async  (req, res) => {
    //revisar si hay errores
    const errores = validationResult(req);
    if(!errores.isEmpty()) {
        return res.status(400).json({ errores: errores.array() });
    }

    // extrae email ypassword
    const { email, password } = req.body;

    try {
        //revisar que sea un usuario registado
        let usuario = await Usuario.findOne({ email });
        if(!usuario) {
            return res.status(400).json({ msg: 'Usuario y/o Password incorrecto(s)' });
        } 

        // revisar el password
        const passCorrecto = await bcryptjs.compare(password, usuario.password);
        if(!passCorrecto) {
            return res.status(400).json({ msg: 'Usuario y/o Password incorrecto(s)'});
        }

        //si todo es correcto, crear y firmar el JWT
        const payload = {
            usuario: {
                id: usuario.id
            }
        };

        jwt.sign(payload, process.env.SECRETA, {
            expiresIn: 3600 //segundos
        }, (error, token) => {  
            if(error) throw error;

            //mensaje de confirmación
            res.json({ token });
        });

    } catch(error) {
        console.log(error)
    }

}

//obtiene los datos del usuario que se autentica
exports.usuarioAutenticado = async (req, res) => {
    try {
        //req.usuario.id lo estamos guardando en el middleware
        //con .select('-password') se le dice que no nos regrese el password
        const usuario = await Usuario.findById(req.usuario.id).select('-password');
        res.json({ usuario })
    } catch (error) {
        console.log(error);
        res.status(500).json({ msg: 'Hubo un error' });
    }
};
