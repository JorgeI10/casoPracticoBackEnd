const Inventario = require('../models/Inventario');
const Sucursal = require('../models/Sucursal');
const Producto = require('../models/Producto');

//obtiene las inventarios
exports.obtenerInventarios = async (req, res) => {
    try {
        //obtener los inventarios
        const inventarios = await Inventario.find();

        //obtener las sucursales
        const sucursales = await Sucursal.find();

        //obtener los inventarios
        const productos = await Producto.find();

        const inventariosCompleto = inventarios.map(inventario => {
            inventario.sucursal = sucursales.find(sucursal => sucursal._id.equals(inventario.sucursal));
            inventario.producto = productos.find(producto => producto._id.equals(inventario.producto));
            return inventario;
        });
        
        res.json({ inventarios: inventariosCompleto });

    } catch(error) {
        console.log(error);
        res.status(500).send('Hubo un error');
    }
};

//actualizar inventario
exports.actualizarInventario = async (req, res) => {
    try {
        //extraer el sucursal y productos y comprobar que existe
        const { cantidad } = req.body;

        //revisar si la inventario existe
        let inventario = await Inventario.findById(req.params.id);

        if(!inventario) {
            return res.status(404).json({ msg: 'No existe el inventario' })
        }

        //extraer sucursal
        const existesSucursales = await Sucursal.findById();

        //extraer productos
        const existenProductos = await Producto.findById();

        //crear objeto con la nueva información
        let nuevoInventario = {};
        nuevoInventario.cantidad = parseInt(inventario.cantidad) + parseInt(cantidad);

        // Guardar la inventario
        inventario = await Inventario.findOneAndUpdate({ _id: req.params.id }, nuevoInventario, { new: true });

        res.json({ inventario });

    } catch (error) {
        console.log(error);
        res.status(500).send('Hubo un error');
    }
};

exports.eliminarInventario = async (req, res) => {
    try {
        //extraer el sucursal y comprobar que existe
        const { producto } = req.query;

        if(!inventario) {
            return res.status(404).json({ msg: 'No existe el inventario' })
        }

        // eliminar 
        inventario = await Inventario.findByIdAndRemove({ producto: producto });

        res.json({ msg: 'Inventario eliminado' });

    } catch (error) {
        console.log(error);
        res.status(500).send('Hubo un error');
    }
};

//hacer venta
exports.hacerVenta = async (req, res) => {
    try {

        const { productos } = req.body;

        //obtener los inventarios
        const inventarios = await Inventario.find();

        for(let i = 0; i < productos.length; i++) {
            //revisar si la inventario existe
            let inventario = inventarios.find(inventario =>
                inventario.sucursal.equals(productos[i].sucursal) && inventario.producto.equals(productos[i].producto)
            );
            if(!inventario) {
                return res.status(404).json({ msg: 'No existe el inventario' })
            }
            //crear objeto con la nueva información
            let nuevoInventario = inventario;
            nuevoInventario.cantidad = parseInt(inventario.cantidad) - parseInt(productos[i].cantidad);
            // Guardar la inventario
            inventario = await Inventario.findOneAndUpdate({ _id: inventario._id }, nuevoInventario, { new: true });
        }
        
        const newInventarios = await Inventario.find();

        res.json({ inventario: newInventarios });

    } catch (error) {
        console.log(error);
        res.status(500).send('Hubo un error');
    }
};