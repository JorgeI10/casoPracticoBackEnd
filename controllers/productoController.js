const Producto = require('../models/Producto');
const Sucursal = require('../models/Sucursal');
const Inventario = require('../models/Inventario');
const { validationResult } = require('express-validator')

exports.crearProducto = async (req, res) => {

    //revisar si hay errores
    const errores = validationResult(req);
    if(!errores.isEmpty()) {
        return res.status(400).json({ errores: errores.array() });
    }

    try {
        //crear una producto nuevo
        const producto = new Producto(req.body);
        //guardar el creador
        producto.creador = req.usuario.id;
        producto.save();

        const sucursales = await Sucursal.find();
        for(let i = 0; i < sucursales.length; i++) {
            const inventario = new Inventario({ sucursal: sucursales[i]._id, producto: producto._id, cantidad: 0 });
            inventario.save();
        }
        res.json(producto);

    } catch(error) {
        console.log(error);
        res.status(500).send('Hubo un error');
    }
};

//obtiene todos los productos del usuario actual
exports.obtenerProductos = async (req, res) => {
    try {
        const productos = await Producto.find();
        res.json({ productos });
    } catch(error) {
        console.log(error);
    }
};

// actualiza una producto
exports.actualizarProducto = async(req, res) => {

    //revisar si hay errores
    const errores = validationResult(req);
    if(!errores.isEmpty()) {
        return res.status(400).json({ errores: errores.array() });
    }

    //extraer la info de la producto
    const { nombre, codigo, precio } = req.body;
    const nuevoProducto = {};

    if(nombre) {
        nuevoProducto.nombre = nombre;
    }

    if(codigo) {
        nuevoProducto.codigo = codigo;
    }

    if(precio) {
        nuevoProducto.precio = precio;
    }

    try {
        //revisar id
        let producto = await Producto.findById(req.params.id);
        
        //verificar si existe la producto
        if(!producto) {
            return res.status(404).json({ msg: 'Producto no encontrada'});
        }

        //actualizar
        producto = await Producto.findByIdAndUpdate({ _id: req.params.id }, { $set: nuevoProducto }, { new: true });
        res.json({ producto });

    } catch(error) {
        console.log(error);
        res.status(500).send('Error en el servidor');
    }
};

//elimina una producto ór su id
exports.eliminarProducto = async (req, res) => {
    try {
        //revisar id
        let producto = await Producto.findById(req.params.id);
                
        //verificar si existe la producto
        if(!producto) {
            return res.status(404).json({ msg: 'Producto no encontrada'});
        }

        //eliminar producto
        await Producto.findOneAndRemove({ _id: req.params.id });
        res.json({ msg: 'Producto eliminado' });

    } catch(error) {
        console.log(error);
        res.status(500).send('Error en el servidor');
    }
};