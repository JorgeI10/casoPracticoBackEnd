const Sucursal = require('../models/Sucursal');
const Producto = require('../models/Producto');
const Inventario = require('../models/Inventario');
const { validationResult } = require('express-validator')

exports.crearSucursal = async (req, res) => {

    //revisar si hay errores
    const errores = validationResult(req);
    if(!errores.isEmpty()) {
        return res.status(400).json({ errores: errores.array() });
    }

    try {
        //crear una sucursal nueva
        const sucursal = new Sucursal(req.body);
        //guardar el creador
        sucursal.creador = req.usuario.id;
        sucursal.save();
        //obtener productos
        const productos = await Producto.find();
        //guardar existencias 0
        for(let i = 0; i < productos.length; i++) {
            const inventario = new Inventario({ sucursal: sucursal._id, producto: productos[i]._id, cantidad: 0 });
            inventario.save();
        }

        res.json(sucursal);

    } catch(error) {
        console.log(error);
        res.status(500).send('Hubo un error');
    }
};

//obtiene todos los sucursales del usuario actual
exports.obtenerSucursales = async (req, res) => {
    try {
        const sucursales = await Sucursal.find();
        res.json({ sucursales });
    } catch(error) {
        console.log(error);
    }
};

// actualiza una sucursal
exports.actualizarSucursal = async(req, res) => {

    //revisar si hay errores
    const errores = validationResult(req);
    if(!errores.isEmpty()) {
        return res.status(400).json({ errores: errores.array() });
    }

    //extraer la info de la sucursal
    const { nombre } = req.body;
    const nuevaSucursal = {};

    if(nombre) {
        nuevaSucursal.nombre = nombre;
    }

    try {
        //revisar id
        let sucursal = await Sucursal.findById(req.params.id);
        
        //verificar si existe la sucursal
        if(!sucursal) {
            return res.status(404).json({ msg: 'Sucursal no encontrada'});
        }

        //actualizar
        sucursal = await Sucursal.findByIdAndUpdate({ _id: req.params.id }, { $set: nuevaSucursal }, { new: true });
        res.json({ sucursal });

    } catch(error) {
        console.log(error);
        res.status(500).send('Error en el servidor');
    }
};

//elimina una sucursal ór su id
exports.eliminarSucursal = async (req, res) => {
    try {
        //revisar id
        let sucursal = await Sucursal.findById(req.params.id);
                
        //verificar si existe la sucursal
        if(!sucursal) {
            return res.status(404).json({ msg: 'Sucursal no encontrada'});
        }

        //eliminar sucursal
        await Sucursal.findOneAndRemove({ _id: req.params.id });
        res.json({ msg: 'Sucursal eliminado' });

    } catch(error) {
        console.log(error);
        res.status(500).send('Error en el servidor');
    }
};