const express = require('express');
const conectarDB = require('./config/db');
const cors = require('cors');

//crear el servidor
const app = express();

//conectar la DB
conectarDB();

//habilitar cors
var allowedOrigins = [
    'http://localhost:3000',
    'localhost:3000',
    'http://localhost:3000/',
    'localhost:3000/',
    'http://localhost:3000/crear-cuenta',
    'localhost:3000/crear-cuenta',
    'http://127.0.0.1:3000',
    '127.0.0.1:3000',
    'http://127.0.0.1:3000/',
    '127.0.0.1:3000/',
    'http://127.0.0.1:3000/crear-cuenta',
    '127.0.0.1:3000/crear-cuenta'
];

app.use(cors({
    origin: function(origin, callback) {
        if(allowedOrigins.indexOf(origin) === -1) {
            var msg = 'La política de CORS para este sitio no '+
                        'permite el acceso desde el origen especificado.';
            return callback(new Error(msg), false);
        }
        return callback(null, true);
    }
}));

//habilitar express.json
app.use(express.json({ extended: true }));

//puerto de la app
const port = process.env.PORT || 4000;

//importar rutas
app.use('/api/usuarios', require('./routes/usuarios'));
app.use('/api/auth', require('./routes/auth'));
app.use('/api/sucursales', require('./routes/sucursales'));
app.use('/api/productos', require('./routes/productos'));
app.use('/api/inventarios', require('./routes/inventarios'));

//arrancar la app
app.listen(port, () => {
    console.log(`El servidor está corriendo en el puerto ${port}.`)
});
