const mongoose = require('mongoose');

const InventarioSchema = mongoose.Schema({
    sucursal: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref: 'Sucursal'
    },
    producto: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref: 'Producto'
    },
    cantidad: {
        type: Number,
        required: true,
        trim: true
    }
});

module.exports = mongoose.model('Inventario', InventarioSchema);