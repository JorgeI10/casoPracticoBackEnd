const express = require('express');
const router = express.Router();
const inventarioController = require('../controllers/inventarioController');
const auth = require('../middleware/auth');
const { check } = require('express-validator');

// api/inventarios
//obtener inventarios 
router.get('/', 
    auth,
    inventarioController.obtenerInventarios
);

//actualizar inventario
router.put('/:id', 
    auth,
    [
        check('sucursal', 'La sucursal es obligatoria').not().isEmpty(),
        check('proyecto', 'El proyecto es obligatorio').not().isEmpty(),
        check('cantidad', 'La cantidad es obligatoria es obligatorio').not().isEmpty()
    ],
    inventarioController.actualizarInventario
);

//realiza venta 
router.post('/venta', 
    auth,
    inventarioController.hacerVenta
);

module.exports = router;