const express = require('express');
const router = express.Router();
const productoController = require('../controllers/productoController');
const auth = require('../middleware/auth');
const { check } = require('express-validator');

// crea productos
// api/productos
router.post('/',
    auth,
    [
        check('nombre', 'El nombre del producto es obligatorio').not().isEmpty(),
        check('precio', 'El precio del producto es obligatorio').not().isEmpty()
    ],
    productoController.crearProducto
);

//obtener todas los productos
router.get('/', auth, productoController.obtenerProductos);

//actuaizar producto por id
router.put('/:id',
    auth,
    [
        check('nombre', 'El nombre del producto es obligatorio').not().isEmpty(),
        check('precio', 'El precio del producto es obligatorio').not().isEmpty()
    ],
    productoController.actualizarProducto
);

//eliminar un producto
router.delete('/:id', auth, productoController.eliminarProducto);

module.exports = router;