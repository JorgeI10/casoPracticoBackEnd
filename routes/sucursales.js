const express = require('express');
const router = express.Router();
const sucursalController = require('../controllers/sucursalController');
const auth = require('../middleware/auth');
const { check } = require('express-validator');

// crea sucursales
// api/sucursales
router.post('/',
    auth,
    [
        check('nombre', 'El nombre de la sucursal es obligatorio').not().isEmpty()
    ],
    sucursalController.crearSucursal
);

//obtener todas los sucursales
router.get('/', auth, sucursalController.obtenerSucursales);

//actuaizar sucursal por id
router.put('/:id',
    auth,
    [
        check('nombre', 'El nombre de la sucursal es obligatorio').not().isEmpty()
    ],
    sucursalController.actualizarSucursal
);

//eliminar un sucursal
router.delete('/:id', auth, sucursalController.eliminarSucursal);

module.exports = router;